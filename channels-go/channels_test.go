package main

import (
	"fmt"
)

func ExampleInitial() {
	ch := initial()
	fmt.Println(<-ch)
	// Output: 1
}

func ExampleAntsSimple() {
	in := initial()
	out := ants(in)

	fmt.Println(<-out)
	fmt.Println(<-out)
	// Output:
	// 1
	// 1
}

func ExampleAntsComplex() {
	in := make(chan int)
	out := ants(in)

	go func() {
		// Input 9th row
		for _, v := range []int{1, 2, 2, 1, 3, 1, 1, 1, 2, 1, 3, 1, 1, 3} {
			in <- v
		}
		close(in)
	}()

	// Output 10th row
	for v := range out {
		fmt.Print(v)
	}
	// Output:
	// 11221131132111311231
}

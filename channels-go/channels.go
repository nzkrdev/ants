package main

import (
	"fmt"
	"os"
	"strconv"
	"time"
)

func initial() <-chan int {
	ch := make(chan int)
	go func() {
		ch <- 1
		close(ch)
	}()
	return ch
}

func ants(in <-chan int) <-chan int {
	out := make(chan int)
	go func() {
		v, n := <-in, 1
		for temp := range in {
			if temp == v {
				n++
				continue
			}
			out <- v
			out <- n
			v, n = temp, 1
		}
		out <- v
		out <- n
		close(out)
	}()
	return out
}

func main() {
	n, err := strconv.Atoi(os.Args[1])
	if err != nil {
		panic(err)
	}

	in := initial()
	out := in
	for i := 0; i < n; i++ {
		out = ants(in)
		in = out
	}

	start := time.Now()
	for v := range out {
		fmt.Print(v)
	}
	fmt.Println()
	fmt.Printf("Elapsed: %f seconds\n", time.Since(start).Seconds())
}

channels-go
===========

* Author: @microamp

고루틴과 채널로 풀어보는 개미수열

Requirements
------------

Go 1.10에서 구현하였습니다

Quick Start
-----------

다음과 같이 컴파일 하면

```
$ go build channels.go
```

`channels`라 하는 바이너리 파일이 생성됩니다

그 후 다음과 같이 10번째 개미수열을 출력합니다

```
$ ./channels 9
```

Running Tests
-------------

```
go test
```

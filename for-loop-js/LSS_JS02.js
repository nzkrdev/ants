var currentArray = [];
var nextArray = [];

function printLevel(level) {
    console.log("Level " + level);
    return;
}

function printArray(array) {
    console.log(":" + array);
    return;
}

function pushToNextArray(number, count) {
    nextArray.push(number);
    nextArray.push(count);
    return;
}

function ant(n) {
    if (n === 1) {
        // Assign 1 to start
        currentArray.push(1);
        return currentArray;
    }
    else {
        count = 1;
        number = currentArray[0];

        for (var i = 1; i < currentArray.length; i++) {
            if (currentArray[i] === number)
                count++;
            else {
                pushToNextArray(number, count);
                number = currentArray[i];
                count = 1;
            }
        }
        pushToNextArray(number, count);

        currentArray = nextArray.slice();
        nextArray = [];

        return currentArray;
    }
}

function test() {
    test = new Array();
    test[1] = [1];
    test[2] = [1, 1];
    test[3] = [1, 2];
    test[4] = [1, 1,2,1];
    test[5] = [1, 2, 2, 1, 1, 1];
    test[6] = [1, 1, 2, 2, 1, 3];
    test[7] = [1, 2, 2, 2, 1, 1, 3, 1];
    test[8] = [1, 1, 2, 3, 1, 2, 3, 1, 1, 1];
    test[9] = [1, 2, 2, 1, 3, 1, 1, 1, 2, 1, 3, 1, 1, 3];
    test[10] = [1, 1, 2, 2, 1, 1, 3, 1, 1, 3, 2, 1, 1, 1, 3, 1, 1, 2, 3, 1];

    // test level 1 to 10
    for (var n = 1; n <= 10; n++) {
        printLevel(n);
        antAnswer = ant(n);
        
        if (test[n].every(function(element, index) {
            return element === antAnswer[index]; 
        })) {
            console.log("ok\n");
        }
        else {
            console.log("error");
            console.log("test:" + test[n]);
            console.log("ant:" + antAnswer);
            console.log("\n");
        }
    }
}

test();
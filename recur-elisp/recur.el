;;; recur.el --- -*- lexical-binding: t; -*-

;;; Commentary:

;; 재귀함수를 이용하여 개미수열 풀기

;;; Code:

(require 'cl-lib)
(require 'seq)

(defun ants-group-ants (ants)
  (when (not (null ants))
    (let* ((taken (seq-take-while (lambda (x) (= x (car ants))) ants))
           (dropped (seq-drop ants (length taken))))
      (cons taken (ants-group-ants dropped)))))

(defun ants-count-ants (grouped)
  (seq-mapcat (lambda (g) (list (car g) (length g))) grouped))

(defun ants--get-nth (ants n)
  (if (zerop n)
      ants
    (let ((grouped (ants-group-ants ants)))
      (ants--get-nth (ants-count-ants grouped) (- n 1)))))

;; Test first 10
(cl-assert (equal (ants--get-nth '(1) 0) '(1)))
(cl-assert (equal (ants--get-nth '(1) 1) '(1 1)))
(cl-assert (equal (ants--get-nth '(1) 2) '(1 2)))
(cl-assert (equal (ants--get-nth '(1) 3) '(1 1 2 1)))
(cl-assert (equal (ants--get-nth '(1) 4) '(1 2 2 1 1 1)))
(cl-assert (equal (ants--get-nth '(1) 5) '(1 1 2 2 1 3)))
(cl-assert (equal (ants--get-nth '(1) 6) '(1 2 2 2 1 1 3 1)))
(cl-assert (equal (ants--get-nth '(1) 7) '(1 1 2 3 1 2 3 1 1 1)))
(cl-assert (equal (ants--get-nth '(1) 8) '(1 2 2 1 3 1 1 1 2 1 3 1 1 3)))
(cl-assert (equal (ants--get-nth '(1) 9) '(1 1 2 2 1 1 3 1 1 3 2 1 1 1 3 1 1 2 3 1)))

;; Display first 10
(print "Displaying the first 10 sequences...")
(seq-doseq (n (number-sequence 0 9))
  (print (ants--get-nth '(1) n)))

;;; recur.el ends here

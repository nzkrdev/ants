recur-elisp
===========

- Author: @microamp

재귀함수를 이용해 풀어보는 개미수열

Requirements
------------

Emacs 26.1에서 Emacs Lisp을 이용해 개발하였습니다

Quick Start
-----------

아래의 커맨드를 터미널에 입력하면

```
emacs --script recur.el
```

다음과 같이 출력되는 것을 볼 수 있습니다

```
"Displaying the first 10 sequences..."

(1)

(1 1)

(1 2)

(1 1 2 1)

(1 2 2 1 1 1)

(1 1 2 2 1 3)

(1 2 2 2 1 1 3 1)

(1 1 2 3 1 2 3 1 1 1)

(1 2 2 1 3 1 1 1 2 1 3 1 1 3)

(1 1 2 2 1 1 3 1 1 3 2 1 1 1 3 1 1 2 3 1)
```

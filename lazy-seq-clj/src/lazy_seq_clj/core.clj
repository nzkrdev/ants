(ns lazy-seq-clj.core)

(defn count-ants [ants]
  (mapcat #((juxt first count) %)
          (partition-by identity ants)))

(defn ant-seqs [initial]
  (iterate count-ants initial))

(defn get-nth [n initial]
  (->> (ant-seqs initial)
       (drop n)
       first))

(defn display-nth [n initial]
  (time (do (doseq [x (get-nth n initial)]
              (pr x))
            (prn))))

(defn -main [& args]
  (let [n (if (empty? args) 9
              (Integer. (first args)))]
    (display-nth n '(1))))

lazy-seq-clj
============

- Author: @microamp

클로져의 지연 시퀀스로 풀어보는 개미수열

Requirements
------------

- [Clojure](https://clojure.org/) 1.9+
- [Leiningen](https://leiningen.org/) (빌드 도구)

Quick Start
-----------

다음과 같이 20번째 개미수열을 출력합니다

```
lein run 19
```

숫자를 표기하지 않았을 경우 10번째 개미수열을 출력합니다

```
lein run
```

Running Tests
-------------

테스트는 다음과 같이 실행합니다

```
lein test
```

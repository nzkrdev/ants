(ns lazy-seq-clj.core-test
  (:require [clojure.test :refer :all]
            [lazy-seq-clj.core :refer :all]))

(deftest test-count-ants
  (testing "Test `count-ants`"
    (are [x y] (= x y)
      (count-ants '(1)) '(1 1)
      (count-ants '(2)) '(2 1)
      (count-ants '(3)) '(3 1)
      (count-ants '(1 2 3)) '(1 1 2 1 3 1)
      (count-ants '(1 2 2 3 3 3)) '(1 1 2 2 3 3))))

(deftest test-ant-seqs
  (testing "Testing `ant-seqs`"
    ;; Test first 10 sequences
    (is
     (= (take 10 (ant-seqs '(1)))
        '((1)
          (1 1)
          (1 2)
          (1 1 2 1)
          (1 2 2 1 1 1)
          (1 1 2 2 1 3)
          (1 2 2 2 1 1 3 1)
          (1 1 2 3 1 2 3 1 1 1)
          (1 2 2 1 3 1 1 1 2 1 3 1 1 3)
          (1 1 2 2 1 1 3 1 1 3 2 1 1 1 3 1 1 2 3 1))))))

(deftest test-get-nth
  (testing "Testing `get-nth`"
    (is
     (= (get-nth 9 '(1))
        '(1 1 2 2 1 1 3 1 1 3 2 1 1 1 3 1 1 2 3 1)))))

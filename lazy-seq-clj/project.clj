(defproject lazy-seq-clj "0.1.0-SNAPSHOT"
  :description "개미수열"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]]
  :main lazy-seq-clj.core)

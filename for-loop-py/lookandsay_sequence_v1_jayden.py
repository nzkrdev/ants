#### Look-and-Say Sequence ####

# Previous result
prev = [] 

for i in range(0,10):   # Take 10th sequence
    curr = []           # Initialising current result every looping
    cnt = 0             # Count same number
    
    # Initial setting: [1]
    if i == 0:
        prev.append(1)
        print(prev)
    elif i > 0:        
        curr.append(prev[0])            # First num is always 1 = prev[0]
        prev_len = len(prev)            # For looping till previous length
        head = 0                        # Indicate Next index after making sum
        j = 0
        while j < prev_len:
            if prev[head] == prev[j]:   # Sum of the numbers until it comes same num
                cnt += 1
                j += 1
            else:                       # prev[head] != prev[j]
                curr.append(cnt)        # set cnt on the next index of current array
                curr.append(prev[j])    # set next prev num on the next index of current array
                head = j                # set the same index in head and j
                cnt = 0                 # initialising cnt
            if j == prev_len:           # set cnt on the last index 
                curr.append(cnt)
        print(curr)
        prev = curr                     # copy current list to previous
        prev_len = len(prev)            # set the previous length


(* Implementation starts here *)

fun count_ants ants =
    let
        fun f (ants, ant, n) =
            case ants
             of [] => ant :: n :: []
              | x :: xs => if x = ant
                           then f (xs, x, n + 1)
                           else ant :: n :: f (xs, x, 1)
    in
        f (tl ants, hd ants, 1)
    end

fun list_ants (n, ants) =
    case n
     of 0 => ants
      | _ => list_ants (n - 1, count_ants ants)

(* Ants but backwards, until the number of items in the sequence is an odd number *)

exception UnbalancedPairs
exception NotAPair

fun is_odd_length lst =
    length lst mod 2 = 1

fun repeat (x, n) =
    if n = 0
    then []
    else x :: repeat (x, n - 1)

fun get_pairs lst =
    case lst
     of [] => []
      | a :: b :: pairs => [a, b] :: get_pairs pairs
      | _ => raise UnbalancedPairs

fun concat pairs =
    case pairs
     of [] => []
      | [x, n] :: pairs' => repeat (x, n) @ concat pairs'
      | _ => raise NotAPair

fun backwards_ants ants =
    if is_odd_length ants
    then ants
    else backwards_ants (concat (get_pairs ants))

(* Do some testing, expecting the following to be all true *)

val initial = [1]

val test01 = list_ants (0, initial) = [1]
val test02 = list_ants (1, initial) = [1, 1]
val test03 = list_ants (2, initial) = [1, 2]
val test04 = list_ants (3, initial) = [1, 1, 2, 1]
val test05 = list_ants (4, initial) = [1, 2, 2, 1, 1, 1]
val test06 = list_ants (5, initial) = [1, 1, 2, 2, 1, 3]
val test07 = list_ants (6, initial) = [1, 2, 2, 2, 1, 1, 3, 1]
val test08 = list_ants (7, initial) = [1, 1, 2, 3, 1, 2, 3, 1, 1, 1]
val test09 = list_ants (8, initial) = [1, 2, 2, 1, 3, 1, 1, 1, 2, 1, 3, 1, 1, 3]
val test10 = list_ants (9, initial) = [1, 1, 2, 2, 1, 1, 3, 1, 1, 3, 2, 1, 1, 1, 3, 1, 1, 2, 3, 1]

val test11 = backwards_ants [1] = [1]
val test12 = backwards_ants [1, 1] = [1]
val test13 = backwards_ants [1, 2] = [1]
val test14 = backwards_ants [1, 1, 2, 1] = [1]
val test15 = backwards_ants [1, 2, 2, 1, 1, 1] = [1]
val test16 = backwards_ants [1, 1, 2, 2, 1, 3] = [1]
val test17 = backwards_ants [1, 2, 2, 2, 1, 1, 3, 1] = [1]
val test18 = backwards_ants [1, 1, 2, 3, 1, 2, 3, 1, 1, 1] = [1]
val test19 = backwards_ants [1, 2, 2, 1, 3, 1, 1, 1, 2, 1, 3, 1, 1, 3] = [1]
val test20 = backwards_ants [1, 1, 2, 2, 1, 1, 3, 1, 1, 3, 2, 1, 1, 1, 3, 1, 1, 2, 3, 1] = [1]

val test31 = backwards_ants [6, 6, 6] = [6, 6, 6]
val test32 = backwards_ants [6, 1, 6, 1, 6, 1] = [6, 6, 6]
val test33 = backwards_ants [6, 1, 1, 1, 6, 1, 1, 1, 6, 1, 1, 1] = [6, 6, 6]
val test34 = backwards_ants [6, 1, 1, 3, 6, 1, 1, 3, 6, 1, 1, 3] = [6, 6, 6]
val test35 = backwards_ants [6, 1, 1, 2, 3, 1, 6, 1, 1, 2, 3, 1, 6, 1, 1, 2, 3, 1] = [6, 6, 6]
val test36 = backwards_ants [6, 1, 1, 2, 2, 1, 3, 1, 1, 1, 6, 1, 1, 2, 2, 1, 3, 1, 1, 1, 6, 1, 1, 2, 2, 1, 3, 1, 1, 1] = [6, 6, 6]

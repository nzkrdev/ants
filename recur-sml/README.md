recur-sml
=========

재귀 함수로 풀어보는 개미수열. [ML](https://en.wikipedia.org/wiki/ML_(programming_language)) 계열의 언어인 Standard ML로 구현하였습니다. ML은 정적 언어이고 엄격한 컴파일러를 가지고 있지만 타입 추론을 통해 굉장히 간결한 프로그램을 짤 수 있습니다.

Requirements
------------

* [Standard ML of New Jersey](http://www.smlnj.org/): `v110.x`에서 구현 및 테스트 하였습니다

Quick Start
-----------

`sml` REPL에서 다음과 같이 파일을 로딩합니다.

```
$ sml
Standard ML of New Jersey v110.83 [built: ...]
- use "recur_sml.sml";
```

테스트의 값은 다 `true`여야 합니다.

```
[opening recur_sml.sml]
val count_ants = fn : int list -> int list
val list_ants = fn : int * int list -> int list
val initial = [1] : int list
val test01 = true : bool
val test02 = true : bool
val test03 = true : bool
val test04 = true : bool
val test05 = true : bool
val test06 = true : bool
val test07 = true : bool
val test08 = true : bool
val test09 = true : bool
val test10 = true : bool
val it = () : unit
```

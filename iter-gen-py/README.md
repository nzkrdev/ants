iter-gen-py
===========

- Author: @microamp

이터레이터와 제너러이터를 이용해 풀어보는 개미수열

Requirements
------------

- 파이썬 3.6을 이용하여 개발되었습니다

Quick Start
-----------

아래 커맨드를 이용하여 각각 1번째, 10번째, 20번째, 30번째, 40번째 그리고 50번째 개미수열을 출력합니다

```
$ python iter_gen.py 0
$ python iter_gen.py 9
$ python iter_gen.py 19
$ python iter_gen.py 39
$ python iter_gen.py 49
```

Running Tests
-------------

테스트는 다음과 같이 돌립니다

```
$ python tests.py
```

# -*- coding: utf-8 -*-

import itertools


def count_ants(k_and_group):
    k, group = k_and_group
    yield k
    yield sum(1 for x in group)


def ants_gen(row):
    while True:
        yield row
        grouped = itertools.groupby(row)
        counted = map(count_ants, grouped)
        row = itertools.chain.from_iterable(counted)


def get_nth(n, initial):
    return next(itertools.islice(ants_gen(initial), n, None))


def display_nth(n):
    for x in get_nth(n, iter([1])):
        print(x, end="")
    print()


if __name__ == "__main__":
    import sys
    import time

    started = time.time()
    display_nth(int(sys.argv[1]))
    print("Elapsed: %.4f seconds" % (time.time() - started))

# -*- coding: utf-8 -*-

import itertools
import unittest

import iter_gen


class TestAnts(unittest.TestCase):
    def test_count_ants_simple(self):
        input = itertools.groupby(iter([1]))

        output = iter_gen.count_ants(next(input))
        self.assertListEqual(list(output), [1, 1])

    def test_count_ants_complex(self):
        input = itertools.groupby(iter([1, 2, 2, 3, 3, 3, 1]))

        output1 = iter_gen.count_ants(next(input))
        self.assertListEqual(list(output1), [1, 1])

        output2 = iter_gen.count_ants(next(input))
        self.assertListEqual(list(output2), [2, 2])

        output3 = iter_gen.count_ants(next(input))
        self.assertListEqual(list(output3), [3, 3])

        output4 = iter_gen.count_ants(next(input))
        self.assertListEqual(list(output4), [1, 1])

    def test_get_nth_simple(self):
        n, initial = 0, iter([1, 1, 1])
        output1 = iter_gen.get_nth(n, initial)
        self.assertEqual(list(output1), [1, 1, 1])

        n, initial = 1, iter([1, 1, 1])
        output2 = iter_gen.get_nth(n, initial)
        self.assertEqual(list(output2), [1, 3])

        n, initial = 2, iter([1, 1, 1])
        output3 = iter_gen.get_nth(n, initial)
        self.assertEqual(list(output3), [1, 1, 3, 1])

    def test_get_first_10(self):
        n = range(10)
        expected = [
            [1],  # 1st
            [1, 1],  # 2nd
            [1, 2],  # 3rd
            [1, 1, 2, 1],  # 4th
            [1, 2, 2, 1, 1, 1],  # 5th
            [1, 1, 2, 2, 1, 3],  # 6th
            [1, 2, 2, 2, 1, 1, 3, 1],  # 7th
            [1, 1, 2, 3, 1, 2, 3, 1, 1, 1],  # 8th
            [1, 2, 2, 1, 3, 1, 1, 1, 2, 1, 3, 1, 1, 3],  # 9th
            [1, 1, 2, 2, 1, 1, 3, 1, 1, 3, 2, 1, 1, 1, 3, 1, 1, 2, 3, 1],  # 10th
        ]

        for n, expected in zip(n, expected):
            output = iter_gen.get_nth(n, iter([1]))
            self.assertListEqual(list(output), expected)


if __name__ == "__main__":
    unittest.main()
